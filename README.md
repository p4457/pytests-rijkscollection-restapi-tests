# pytests-rijks-restAPI-pytests

#### This Repo has the test set for few API's collection related to Rijksmuseum
for more information
https://data.rijksmuseum.nl/object-metadata/api/#access-to-apis

### Note:
#### Few tests are failing because of an unexpected behaviour, ie., the response is seen 200 even there is a typo with parameter names in the request


# Pytest - API testing with Python `requests`
 
#### Pytest is a mature full-featured Python testing frame that helps you write and run tests in Python.

#### The `requests` module allows you to send HTTP requests using Python.

## Getting started

* To download and install `pytest`, run this command from the terminal : `pip install pytest`
* To download and install `requests`, run this command from the terminal : `pip install requests`

To ensure all dependencies are resolved in a CI environment, in one go, add them to a `requirements.txt` file.
* Then run the following command : `pip install -r requirements.txt`

By default, pytest only identifies the file names starting with `test_` or ending with `_test` as the test files.

Pytest requires the test method names to start with `test`. All other method names will be ignored even if we explicitly ask to run those methods.

A sample test below :

```python

# def test_fetch_user() :
#     path = "api/users/2"
#     response = requests.get(url=baseUrl+path)
#     responseJson = json.loads(response.text)
#     assert response.status_code == 200
#     assert jsonpath.jsonpath(responseJson,'$.data.first_name')[0] == 'Janet'
#     assert jsonpath.jsonpath(responseJson,'$.data.id')[0] == 2

```
## Running tests

tests are contained inside a folder 'Tests', then run the following command : `pytest` 

To generate xml results, run the following command : `pytest --junitxml="../results/result.xml"`

To generate html  results, run the following command : `pytest -v --html=../results/report.html`



For more on Pytest, 
go [here.](https://docs.pytest.org/en/stable/)

#### The tests are integrated with gitlab pipeline