import requests
from config import configtest


class GetCollectionDetails:
    def __init__(self, baseurl):
        self.baseurl = baseurl

    def get_collection_details(self, params):
        return requests.get(url=self.baseurl + configtest.GET_COLLECTION_END_POINT + '/'
                                + configtest.VALID_COLLECTION_ID,
                            params=params)

    def get_collection_details_invalid_collection_id(self, params):
        return requests.get(url=self.baseurl + configtest.GET_COLLECTION_END_POINT + '/'
                                + configtest.INVALID_COLLECTION_ID,
                            params=params)
