import requests
from config import configtest


class GetCollectionImage:
    def __init__(self, baseurl):
        self.baseurl = baseurl

    def get_collection_image(self, params):
        return requests.get(url=self.baseurl + configtest.GET_COLLECTION_END_POINT + '/' +
                                configtest.VALID_COLLECTION_ID + '/' + 'tiles', params=params)

    def get_collection_image_invalid_collection_id(self, params):
        return requests.get(url=self.baseurl + configtest.GET_COLLECTION_END_POINT + '/' +
                                configtest.INVALID_COLLECTION_ID + '/' + 'tiles', params=params)
